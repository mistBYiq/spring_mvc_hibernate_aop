create table employees (
  id int not null generated always as identity primary key,
  name varchar(15),
  surname varchar(25),
  department varchar(20),
  salary int
) ;

insert (name, surname, department, salary)
values
	('Zaur', 'Tregulov', 'IT', 500),
	('Oleg', 'Ivanov', 'Sales', 700),
	('Nina', 'Sidorova', 'HR', 850);








create table m_users (
id bigint not null generated always as identity primary key,
role_id bigint,
location_id bigint,
name varchar(50),
surname varchar(50),
gender varchar(10),
data_birth date,
email varchar(64),
phone bigint,
login varchar(50),
password varchar(50),
balance bigint,
created timestamp,
changed timestamp,
status varchar(20),
constraint fk_role_m_users foreign key (role_id) references role (id),
constraint fk_location_m_users foreign key (location_id) references location (id)
);

create table role (
id bigint not null generated always as identity primary key,
system_role varchar(20),
user_id bigint,
constraint fk_m_users_role foreign key (user_id) references m_users (id)
);

create table location (
id bigint not null generated always as identity primary key,
name_place varchar(100),
northern_latitude real,
western_longitude real,
country varchar(50),
city varchar(100),
created timestamp,
changed timestamp,
status varchar(20)
);

create table m_photos (
id bigint not null generated always as identity primary key,
name varchar(100),
author_id bigint,
category_id bigint,
location_id bigint,
licence_id bigint,
url varchar(100),
price integer,
created timestamp,
changed timestamp,
status varchar(20),
constraint fk_m_users_m_photos foreign key (author_id) references m_users (id),
constraint fk_category_m_photos foreign key (category_id) references category (id),
constraint fk_location_m_photos foreign key (location_id) references location (id),
constraint fk_licence_m_photos foreign key (licence_id) references licence (id)
);

create table category (
id bigint not null generated always as identity primary key,
name varchar(100),
created timestamp,
changed timestamp,
status varchar(20)
);

create table licence (
id bigint not null generated always as identity primary key,
type varchar(30),
photo_id biging,
created timestamp,
changed timestamp,
status varchar(20),
constraint fk_m_photos_licence foreign key (photo_id) references m_photos (id)
);

create table orders (
id bigint not null generated always as identity primary key,
user_id bigint,
photo_id bigint,
total_price integer,
created timestamp,
changed timestamp,
status varchar(20),
constraint fk_m_users_orders foreign key (user_id) references m_users (id),
constraint fk_m_photos_orders foreign key (photo_id) references m_photos (id)
);

create table keyword (
id bigint not null generated always as identity primary key,
name varchar(50),
created timestamp,
changed timestamp,
status varchar(20)
);

create table l_keyword_photo (
keyword_id bigint not null,
photo_id bigint not null,
primary key(keyword_id, photo_id),
constraint fk_keyword_l_keyword_photo foreign key (keyword_id) references keyword (id),
constraint fk_m_photos_l_keyword_photo foreign key (photo_id) references m_photos (id)
);